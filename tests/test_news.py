from pages.news import News
from tests.base_test import BaseTest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class NewsTests(BaseTest):
    new_title = (By.XPATH, "//*[contains(text(), 'Новини Компанії')]")

    def test_news_button(self):
        news = News(self.driver)
        news.open()
        news.open_all_news()
        title = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located(self.new_title)
        )
        assert title, "Page for all news was not opened"
