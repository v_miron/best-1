from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.base_page import BasePage

class News(BasePage):

    def open_all_news(self):
        open_all_new_button = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Всі новини')]"))
        )
        open_all_new_button.click()
        