class BasePage:

    base_url = "https://best.net.ua/"

    def __init__(self, driver):
        self.driver = driver

    def open(self):
        self.driver.get(self.base_url)
